<?php 
require_once('config.php'); 
?>


<!DOCTYPE html>
<html>
<head>
    <?php require("head.php");?>
</head>
<body>


<?php require("header.php");?>




<?php if (isset($infoGenerali->lights)): ?>
    <?php foreach($infoGenerali->lights as $k=>$luce):?>
    <fieldset data-role="controlgroup" data-type="horizontal">
    <legend><?= $luce->name?>:</legend>
        <input class="pulsante" name="<?=$k?>-radio-choice-h-2" id_refer="<?=$k?>" id="<?=$k?>-radio-choice-h-2a" value="on" <?= ($luce->state->on) ? 'checked="checked"' : '' ?> type="radio">
        <label for="<?=$k?>-radio-choice-h-2a">Accesa</label>
        <input class="pulsante" name="<?=$k?>-radio-choice-h-2" id_refer="<?=$k?>" id="<?=$k?>-radio-choice-h-2b" value="off" <?= ($luce->state->on) ? '':'checked="checked"' ?>type="radio">
        <label for="<?=$k?>-radio-choice-h-2b">Spenta</label>
        <a href="<?= $url?>/lucefull.php?k=<?=$k?>" class="ui-btn ui-shadow ui-corner-all ui-btn-icon-left ui-icon-carat-r">Info</a>
    </fieldset>
    <?php endforeach;?>
<?php else: ?>
        nessuna luce trovata
<?php endif;?>





<?php require("footer.php");?>
</body>
</html>
