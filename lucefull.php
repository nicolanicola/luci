<?php 
require_once('config.php'); 
?>


<!DOCTYPE html>
<html>
<head>
    <?php require("head.php");?>
    <script>
        var lastRange = 0;
    </script>
</head>
<body>


<?php require("header.php");?>


<?php
    $id = isset($_GET["k"]) ? $_GET["k"] : null;
?>

  <?php  if (isset($id)) : ?>

  <?php
    $ret = $HueAPI->loadInfo("lights/1");
    $luce = json_decode($ret);

    if (is_array($luce) &&  isset($luce[0]->error)){
        echo $this->messages[] = $luce[0]->error;
    }else {
?>
    <fieldset data-role="controlgroup" data-type="horizontal">
    <legend><?= $luce->name?>:</legend>
        <input class="pulsante" name="<?=$id?>-radio-choice-h-2" id_refer="<?=$id?>" id="<?=$id?>-radio-choice-h-2a" value="on" <?= ($luce->state->on) ? 'checked="checked"' : '' ?> type="radio">
        <label for="<?=$id?>-radio-choice-h-2a">Accesa</label>
        <input class="pulsante" name="<?=$id?>-radio-choice-h-2" id_refer="<?=$k?>" id="<?=$id?>-radio-choice-h-2b" value="off" <?= ($luce->state->on) ? '':'checked="checked"' ?>type="radio">
        <label for="<?=$id?>-radio-choice-h-2b">Spenta</label>
    </fieldset>
    
    <input type="range" class="pulsante_bri" name="slider-<?=$id?>" id="slider-<?=$id?>" value="<?=$luce->state->bri?>" min="0" max="254" step="1" data-highlight="true" id_refer="<?=$id?>">

    <?php
      $type = "other";
      if ($luce->type == "Extended color light") {$type ="bulb";}
      $rgb =  xyToRGB ($luce->state->xy[0],$luce->state->xy[1], $luce->state->bri,$type);
    ?>
      <input type='text' id="flat<?=$id;?>" value="<?=$rgb?>">
    
    <script>
    var lastMove = 0;
    $("#flat<?=$id;?>").spectrum({
        flat: true,
        showInput: true,
        showInitial:true
    
        ,showPalette: true
        ,hideAfterPaletteSelect : false
        ,showButtons:true
        ,preferredFormat: "hex"
    
    
        ,move: function(color) {
            var d = new Date();
            var n = d.getTime();
            if ((n - lastMove ) > 300){
                console.log(color.toHexString());
                var val = "cmd=cambia_colore&k=<?=$id;?>&color="+color.toHexString();
                sendCmd(val);
                lastMove = n;
            }
    
        }
    
    });
    </script>





    <table data-role="table" id="table-column-toggle" data-mode="columntoggle" class="ui-responsive table-stripe">
     <thead>
       <tr>
         <th data-priority="1">Nome</th>
         <th>Valore</th>
       </tr>
     </thead>
     <tbody>
        <?php foreach($luce as $k1=>$val): ?>

				    <tr>
				    	<th><?=$k1?></th>
              <td><?php 
                    if (!is_object($val)){
                       echo $val;
                    } else {
                        ?>
                            <table class="ui-responsive">
                            <tbody>
                                <?php foreach($val as $k2=>$val1): ?>
                                    <tr>
                                        <th><?=$k2;?></th>
                                        <td>
                                            <?= convertiValore($val1);?>
                                        </td>
                                    </tr>
		                            <?php endforeach; ?>
                            </tbody>
                            </table>
                        <?php
                    }
                ?>
              </td>
				    </tr>	
		        <?php endforeach; ?>
    </tbody>
    </table>






    <?php 
    }?>  
    <?php else:?>
   nessun parametro presente 
  <?php endif;?>


<?php require("footer.php");?>
</body>
</html>
