<?php

class comandi{

  private $cmd;
  private $messages;
  private $status;
  private $details;
  
  public function __construct(){
  }
  public function eseguiComando($cmd){
    $this->messages = array();
    $this->details = array();
    $this->status = "success";
    if (isset($cmd)){
      if ( method_exists($this,$cmd) ){
        $this->$cmd();
      }else {
        $this->messages[] = "il comando passato non è valido";
        $this->status = "error";
      }
    }else {
      $this->messages[] = "nessun comando passato";
      $this->status = "error";
    }

    header('Content-Type: application/json');
    echo json_encode(array('status' => $this->status ,'message'=> implode("<br>",$this->messages) , "details" => implode("<br>",$this->details) ));
  }



  private function luci(){
    $this->details[] = "Start: ".__FILE__." ".__CLASS__." ".__FUNCTION__." ROW: ".__LINE__;
    $this->details[] = "POST: <pre>".var_export($_POST,true)."</pre>";
    $id = isset($_POST["k"]) ? $_POST["k"] : null;
    if (isset($id)){
        global $HueAPI;
        $urlInvio = "lights/".$id;
        $this->details[] = "Send method GET -> ".$urlInvio;
        $actual =json_decode($HueAPI->loadInfo($urlInvio,"","GET"));
        $this->details[] = "Response: <pre>".var_export($actual,true)."</pre>";
        if (is_array($actual) &&  isset($actual[0]->error)){
            $this->status = "error";
            $this->messages[] = $actual[0]->error;
        }else {
          $status = $actual->state->on;
          $cont = array();
          $cont["on"] = !$status;  
          if (isset($_POST["bri"]) && $status==false){
            $cont["bri"] =  (int) $_POST["bri"];
          }
          $urlInvio = "lights/".$id."/state";
          $this->details[] = "Send method PUT -> ".$urlInvio;
          $this->details[] = "<pre>".var_export($cont,true)."</pre>";
          $rethue = $HueAPI->setInfo($urlInvio,json_encode($cont),"PUT");
          $this->messages[] = "stato luce ".$actual->name."  ". (($status===true) ? "spenta" : "accesa");
          $this->details[] = "<pre>".var_export($rethue,true)."</pre>";
          $this->details[] = "end";
        }

    }else {
      $this->status = "error";
      $this->messages[] = "nessun id passato";
    }
  }



  private function spegni_tutto(){
    $this->details[] = "Start: ".__FILE__." ".__CLASS__." ".__FUNCTION__." ROW: ".__LINE__;
    $this->details[] = "POST: <pre>".var_export($_POST,true)."</pre>";
    global $HueAPI;
    $this->details[] = "Send method GET -> lights";
    $luci =json_decode($HueAPI->loadInfo("lights","GET"));
    $this->details[] = "Response: <pre>".var_export($luci,true)."</pre>";
    if (is_array($luci) &&  isset($luci[0]->error)){
        $this->status = "error";
        $this->messages[] = $luci[0]->error;
    }else {
      if (isset($luci)){
        foreach($luci as $k=>$luce){
          if ($luce->state->on){ //controllo invio comando solo se è accesa

            $cont["on"] = false;  
            $urlInvio = "lights/".$k."/state";
            $this->details[] = "Send method PUT -> ".$urlInvio;
            $this->details[] = "<pre>".var_export($cont,true)."</pre>";
            $rethue = $HueAPI->setInfo($urlInvio,json_encode($cont),"PUT");
            $this->messages[] = "stato luce ".$luce->name." spenta ";
            $this->details[] = "<pre>".var_export($rethue,true)."</pre>";
            $this->details[] = "end";
          }
        }
      }
    }
  }

  private function cambia_colore(){
    $this->details[] = "Start: ".__FILE__." ".__CLASS__." ".__FUNCTION__." ROW: ".__LINE__;
    $this->details[] = "POST: <pre>".var_export($_POST,true)."</pre>";
    $id = isset($_POST["k"]) ? $_POST["k"] : null;
    if (isset($id)){
        global $HueAPI;
        $urlInvio = "lights/".$id;
        $this->details[] = "Send method GET -> ".$urlInvio;
        $actual =json_decode($HueAPI->loadInfo($urlInvio,"","GET"));
        $this->details[] = "Response: <pre>".var_export($actual,true)."</pre>";
        if (is_array($actual) &&  isset($actual[0]->error)){
            $this->status = "error";
            $this->messages[] = $actual[0]->error;
        }else {
          $cont = array();
          $cont["on"] = true;
          $cont = array_merge($cont, RGBToXy($_POST["color"])) ; 
//        $cont["xy"] =  $this->RGBToXy($_POST["color"]) ;
//          $cont["bri"] = 255;
          $cont["sat"] = 255;
          unset($cont["bri"]);
          $urlInvio =  "lights/".$id."/state"; 
          $this->details[] = "Send method PUT -> ".$urlInvio;
          $this->details[] = "<pre>".var_export($cont,true)."</pre>";
          $rethue = $HueAPI->setInfo($urlInvio,json_encode($cont),"PUT");
          $this->messages[] = "acceso luce ".$actual->name." ".$_POST["color"];
          $this->details[] = "<pre>".var_export($rethue,true)."</pre>";
          $this->details[] = "end";
        }

    }else {
      $this->status = "error";
      $this->messages[] = "nessun id passato";
    }
  }
  private function setBri(){
    $this->details[] = "Start: ".__FILE__." ".__CLASS__." ".__FUNCTION__." ROW: ".__LINE__;
    $this->details[] = "POST: <pre>".var_export($_POST,true)."</pre>";
    $id = isset($_POST["k"]) ? $_POST["k"] : null;
    if (isset($id)){
        global $HueAPI;
        $urlInvio = "lights/".$id;
        $this->details[] = "Send method GET -> ".$urlInvio;
        $actual = json_decode($HueAPI->loadInfo($urlInvio,"","GET"));
        $this->details[] = "Response: <pre>".var_export($actual,true)."</pre>";
        if (is_array($actual) &&  isset($actual[0]->error)){
            $this->status = "error";
            $this->messages[] = $actual[0]->error;
        }else {
          $cont = array();
          $cont["on"] = true;
          $cont["bri"] = (int) $_POST["bri"];
//        $cont["sat"] = 255;
          $urlInvio = "lights/".$id."/state";
          $this->details[] = "Send method PUT -> ".$urlInvio;
          $this->details[] = "<pre>".var_export($cont,true)."</pre>";
          $rethue = $HueAPI->setInfo($urlInvio,json_encode($cont),"PUT");
          $this->messages[] = "cambiato bri luce ".$actual->name." ".$_POST["bri"];
          $this->details[] = "<pre>".var_export($rethue,true)."</pre>";
          $this->details[] = "end";
        }

    }else {
      $this->status = "error";
      $this->messages[] = "nessun id passato";
    }
  }
}
?>
