
function sendCmd(val){

  $.ajax({
        url: url+"/command.php",
        type: "post",
        data: val,
        datatype: 'json',
        success: function(data){
          console.log(data);

          $('#result_messages').html(data.status +':' + data.message);   
          $('#result_details').html(data.details);   
          $("#result").addClass('msg_'+data.status);
          $("#result").fadeIn(1500);           
                                                    
        },
        error:function(){
           $("#result_messages").html('Errore invio comando');
           $('#result_details').html('');   
           $("#result").addClass('msg_error');
           $("#result").fadeIn(1500);
                                              
        }   
            
  });
}

function setSlider(k,val){
  var val = "cmd=setBri&k="+k+"&bri="+val;
  sendCmd(val);
}

$( document ).ready(function() {



    $(".spegni_tutto").click(function(){
      var val = "cmd=spegni_tutto";
      sendCmd(val);
    });


    $(".pulsante_bri").change(function(){
        var d = new Date();
        var n = d.getTime();
//        console.log(lastRange);
        if ((n - lastRange ) > 300){
            var val = "cmd=setBri&k="+$(this).attr("id_refer")+"&bri="+$(this).val();
            console.log(val);
            sendCmd(val);
            lastRange = n;
        }
    });
    
    $(".pulsante").click(function(){
        var val = "cmd=luci&k="+$(this).attr("id_refer");
        sendCmd(val);
    });

    $(".pulsante_switch").click(function(){
        var val = "cmd=luci&k="+$(this).attr("id_refer")+"&bri=255";
        sendCmd(val);
        $( this  ).children( "img"  ).each(function (){
          if ($(this).hasClass( "attiva"  )){
            $(this).removeClass("attiva");
          }else{

            $(this).addClass("attiva");
          }
        });
    });



});



