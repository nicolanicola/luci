<div data-role="header" data-position="fixed" data-fullscreen="false" >
    <h1>My Php Hue</h1>

	<?php if (is_array($infoGenerali) &&  isset($infoGenerali[0]->error)): ?>
    <a href="#popupBasicStatus" data-icon="gear" data-rel="popup" class="ui-btn-right non_connesso" data-transition="pop">Non Connesso</a>
    <div data-role="popup" id="popupBasicStatus">
    <p>Errore: <?= $infoGenerali[0]->error->description;?></p>
    </div>
	<?php else: ?>
    <a href="#" data-icon="gear" class="ui-btn-right connesso">Connesso</a>
  <?php endif;?>	
    <div data-role="navbar">
        <ul>
            <?php $c = ( $_SERVER["SCRIPT_NAME"] == $url."/index.php"  ) ? 'class="ui-btn-active"' : '' ?>
            <li><a href="<?=$url?>" <?=$c?>>Home</a></li>

            <?php if (isset($infoGenerali->lights)): ?>
                <?php foreach($infoGenerali->lights as $k=>$luce):?>
                    <?php $c = ( $_SERVER["SCRIPT_NAME"] == $url."/luce.php" && $_GET["k"] == $k ) ? 'class="ui-btn-active"' : '' ?>
                    <li><a href="<?=$url;?>/luce.php?k=<?=$k?>" <?=$c?>><?=$luce->name?></a></li>
                <?php endforeach;?>
            <?php endif;?>
        </ul>
    </div><!-- /navbar -->

    <div id="result" data-role="collapsible" data-collapsed="true" data-collapsed-icon="carat-d" data-expanded-icon="carat-u" style="display:none">
        <h4>  <div id="result_messages" > Heading</div> </h4>
        <p id="result_details">I'm the collapsible content. By default I'm closed, but you can click the header to open me.</p>
    </div>
</div>


