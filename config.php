<?php
$url = "/luci";

/*****************
 * Configuration
 * Copy this file as 'config.php' and fill parameters
 *****************/
$bridgeip = "192.168.11.35";
$username = "43b67cb1d3a4a2f924c0d31183efb7";

$appname = "phpMyHue"; // optional

$apiurl = "http://$bridgeip/api/$username";

require_once (__DIR__."/include/comandi.php");
require_once (__DIR__."/include/hueapi.php");
require_once (__DIR__."/include/funzioni.php");




$arFieldDisplay = array(
	"name",
	"zigbeechannel",
	"bridgeid",
	"mac",
	"dhcp",
	"ipaddress",
	"netmask",
	"gateway",
	"proxyaddress",
	"proxyport",
	"UTC",
	"localtime",
	"timezone"
);

global $HueAPI;
$ret = $HueAPI->loadInfo("");
$infoGenerali = json_decode($ret);


?>
