<?php 
require_once('config.php'); 
?>


<!DOCTYPE html>
<html>
<head>
    <?php require("head.php");?>
</head>
<body>


<?php require("header.php");?>

<table data-role="table" id="table-column-toggle" data-mode="columntoggle" class="ui-responsive table-stripe">
     <thead>
       <tr>
         <th data-priority="1">Nome</th>
         <th>Valore</th>
       </tr>
     </thead>
     <tbody>
		    <tr>
			      <td colspan="2" class="intera_riga">Da configurazione</td>
		    </tr>	
		    <tr>
			      <th>Ip Bridge</th>
			      <td><?=$bridgeip?></td>
		    </tr>	
		    <tr>
			      <th>Username</th>
			      <td><?=$username?></td>
		    </tr>	
		    <tr>
			      <td colspan="2" class="intera_riga">Dal Bridge</td>
		    </tr>
	      
        <?php if (isset($infoGenerali->config)): ?>	
            <?php foreach($infoGenerali->config as $k=>$val): ?>

        			<?php if (in_array($k , $arFieldDisplay)  ): ?>
				        <tr>
        					<th><?=$k?></th>
				        	<td><?=$val?></td>
				        </tr>	
			        <?php endif;?>
        		<?php endforeach; ?>
        <?php else: ?>
        		<tr>
        			<th></th>
        			<td>errore nel recuperare i dati</td>
        		</tr>	
        <?php endif;?>
     </tbody>
   </table>



<?php require("footer.php");?>
</body>
</html>
